VERSION ?= latest

REPO = growbyme/java
NAME = java
INSTANCE = default

.PHONY: build push shell run start stop rm release

build:
	docker build -t $(REPO):$(VERSION) ${NAME}/.

push:
	docker push $(REPO):$(VERSION)

shell:
	docker run --rm --name $(NAME)-$(INSTANCE) -i -t $(PORTS) $(VOLUMES) $(ENV) $(REPO):$(VERSION) /bin/bash

run:
	docker run --rm --name $(NAME)-$(INSTANCE) $(PORTS) $(VOLUMES) $(ENV) $(REPO):$(VERSION)

start:
	docker run -d --name $(NAME)-$(INSTANCE) $(PORTS) $(VOLUMES) $(ENV) $(REPO):$(VERSION)

stop:
	docker stop $(NAME)-$(INSTANCE)

clean:
	docker rm $(NAME)-$(INSTANCE)

clean-images:
	@docker rmi `docker images -q -f "dangling=true"`

release: build
	make push -e VERSION=$(VERSION)

default: build
